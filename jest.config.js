/* eslint-disable no-undef */

module.exports = {
  testMatch: ['<rootDir>/src/**/*.test.js'],
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  coverageDirectory: '<rootDir>/generated/coverage',
  verbose: true,
};
