/* eslint-env jest */

import 'core-js/stable';
import 'regenerator-runtime/runtime';

import 'whatwg-fetch';

import './src/browser/polyfill';
