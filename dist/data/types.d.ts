type ID = string;
type URI = string;
type UserRole = "user" | "admin";
type UserAccount = {
    id: ID;
    role: UserRole;
    email: string;
    emailVerified: boolean;
    password: string;
    name: string;
    pictureUrl: URI;
    createdAt: Date;
    updatedAt: Date;
};
type UserProfile = {
    id: ID;
};
type Post = {
    id: ID;
    title: string;
    slug: string;
    body: string;
    pictureUrl: URI;
    createdAt: Date;
    updatedAt: Date;
};
