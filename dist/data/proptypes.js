"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Post = exports.UserProfile = exports.UserAccount = exports.UserRole = exports.URI = exports.ID = void 0;

var PropTypes = _interopRequireWildcard(require("prop-types"));

var CONST = _interopRequireWildcard(require("../common/const"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// eslint-disable-next-line import/no-unresolved
var ID = PropTypes.string;
exports.ID = ID;
var URI = PropTypes.string;
exports.URI = URI;
var UserRole = PropTypes.oneOf(Object.values(CONST.ROLE));
exports.UserRole = UserRole;
var UserAccount = PropTypes.shape({
  id: ID.isRequired,
  role: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  emailVerified: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  pictureUrl: URI.isRequired
});
exports.UserAccount = UserAccount;
var UserProfile = PropTypes.shape({
  id: ID.isRequired
});
exports.UserProfile = UserProfile;
var Post = PropTypes.shape({
  id: ID.isRequired,
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  pictureUrl: URI.isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired
});
exports.Post = Post;
//# sourceMappingURL=proptypes.js.map