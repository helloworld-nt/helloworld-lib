export const ID: PropTypes.Requireable<string>;
export const URI: PropTypes.Requireable<string>;
export const UserRole: PropTypes.Requireable<string>;
export const UserAccount: PropTypes.Requireable<PropTypes.InferProps<{
    id: PropTypes.Validator<string>;
    role: PropTypes.Validator<string>;
    email: PropTypes.Validator<string>;
    emailVerified: PropTypes.Validator<boolean>;
    name: PropTypes.Validator<string>;
    pictureUrl: PropTypes.Validator<string>;
}>>;
export const UserProfile: PropTypes.Requireable<PropTypes.InferProps<{
    id: PropTypes.Validator<string>;
}>>;
export const Post: PropTypes.Requireable<PropTypes.InferProps<{
    id: PropTypes.Validator<string>;
    title: PropTypes.Validator<string>;
    slug: PropTypes.Validator<string>;
    body: PropTypes.Validator<string>;
    pictureUrl: PropTypes.Validator<string>;
    createdAt: PropTypes.Validator<string>;
    updatedAt: PropTypes.Validator<string>;
}>>;
import * as PropTypes from "prop-types";
