"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.email = email;

/**
 * Sanitation
 */

/**
 *
 * @param {string} value
 * @returns {string}
 */
function email(value) {
  return value.trim().toLowerCase();
}
//# sourceMappingURL=sanitize.js.map