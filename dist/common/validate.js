"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.email = email;

/**
 * Validation
 */

/**
 *
 * @param {string} value
 * @returns {boolean}
 */
function email(value) {
  return value.includes('@');
}
//# sourceMappingURL=validate.js.map