export function defineLocale(localeCode: any, defaults?: {}): {
    setTranslations(_translations: any): void;
    updateTranslations(_translations: any): void;
    $t(text: any, ...args: any[]): any;
    number(value: any, options?: {}): any;
    currency(value: any, currency?: any, options?: {}): any;
    date(value: any, options?: {}): any;
    time(value: any, options?: {
        hour: string;
        minute: string;
    }): any;
};
export const TIME_ZONE: string;
export default intl;
declare namespace intl {
    export const en: {
        setTranslations(_translations: any): void;
        updateTranslations(_translations: any): void;
        $t(text: any, ...args: any[]): any;
        number(value: any, options?: {}): any;
        currency(value: any, currency?: any, options?: {}): any;
        date(value: any, options?: {}): any;
        time(value: any, options?: {
            hour: string;
            minute: string;
        }): any;
    };
    import current = en;
    export { current };
}
