/**
 * Sanitation
 */
/**
 *
 * @param {string} value
 * @returns {string}
 */
export function email(value: string): string;
