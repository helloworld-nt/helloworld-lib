/**
 * Transformation
 */
/**
 *
 * @param {string} value
 * @returns {string}
 */
export function slugify(value: string): string;
