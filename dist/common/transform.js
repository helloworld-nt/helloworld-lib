"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.slugify = slugify;

/**
 * Transformation
 */

/**
 *
 * @param {string} value
 * @returns {string}
 */
function slugify(value) {
  return value.normalize('NFD').trim().toLowerCase().replace(/[\u0300-\u036f]/g, '').replace(/[^a-z0-9]+/g, '-').replace(/(^-+)|(-+$)/g, '');
}
//# sourceMappingURL=transform.js.map