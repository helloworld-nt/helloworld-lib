"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encode = encode;
exports.toQueryString = toQueryString;
exports.toFormData = toFormData;
exports.Request = Request;
exports.handle = handle;
exports.events = void 0;

var _events = require("./events");

var _error3 = require("./error");

var _logger = require("./logger");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var Logger = (0, _logger.createLogger)('FetchHelper');
/**
 * Query String and Form Data
 */

/**
 *
 * @param {URLSearchParams|FormData} result
 * @param {string} name
 * @param {*} value
 * @returns {void}
 */

function encode(result, name, value) {
  if (_typeof(value) === 'object' && value) {
    if (value instanceof File) {
      if (result instanceof FormData) {
        result.set(name, value);
      }
    } else if (value instanceof Date) {
      result.set(name, value.toJSON());
    } else if (typeof value.toJSON === 'function') {
      encode(result, name, JSON.parse(value.toJSON()));
    } else if (Array.isArray(value)) {
      for (var index = 0; index < value.length; index += 1) {
        encode(result, "".concat(name, "[").concat(index, "]"), value[index]);
      }
    } else {
      for (var _i = 0, _Object$entries = Object.entries(value); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            val = _Object$entries$_i[1];

        encode(result, "".concat(name, "[").concat(key, "]"), val);
      }
    }
  } else if (typeof value === 'undefined') {
    result.set(name, '');
  } else {
    result.set(name, value);
  }
}
/**
 *
 * @param {Object} data
 * @returns {string}
 */


function toQueryString(data) {
  var result = new URLSearchParams();

  for (var _i2 = 0, _Object$entries2 = Object.entries(data); _i2 < _Object$entries2.length; _i2++) {
    var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
        name = _Object$entries2$_i[0],
        value = _Object$entries2$_i[1];

    encode(result, name, value);
  }

  return result.toString();
}
/**
 *
 * @param {Object} data
 * @returns {FormData}
 */


function toFormData(data) {
  var result = new FormData();

  for (var _i3 = 0, _Object$entries3 = Object.entries(data); _i3 < _Object$entries3.length; _i3++) {
    var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i3], 2),
        name = _Object$entries3$_i[0],
        value = _Object$entries3$_i[1];

    encode(result, name, value);
  }

  return result;
}
/**
 *
 * @param {string} method
 * @param {string} url
 * @param {Object} options
 * @returns {Request}
 */


function Request(method, url) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var route = options.route,
      query = options.query,
      headers = options.headers,
      body = options.body,
      more = _objectWithoutProperties(options, ["route", "query", "headers", "body"]);

  route = route || {};
  query = query || {};
  headers = headers || {};
  Object.entries(route).forEach(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        param = _ref2[0],
        value = _ref2[1];

    url = url.replace(new RegExp(":".concat(param)), encodeURIComponent(value));
  });
  var queryString = toQueryString(query);
  url += queryString ? "?".concat(queryString) : '';
  headers.Accept = 'application/json';

  if (body && _typeof(body) === 'object') {
    if (body instanceof FormData) {
      headers['Content-Type'] = 'multipart/form-data';
    } else if (body instanceof URLSearchParams) {
      headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
    } else if (toString.call(body) === '[object Object]' || toString.call(body) === '[object Array]') {
      headers['Content-Type'] = 'application/json';
      body = JSON.stringify(body);
    }
  }

  var request = new globalThis.Request(url, _objectSpread({
    method: method,
    headers: headers,
    body: body
  }, more));
  return request;
}
/**
 * Response Listeners
 */


var events = new _events.EventEmitter();
/**
 * Handle fetch request, converts JSON to Object and throws error for non-success statuses
 *
 * @param {Promise<Response>} fetchPromise
 * @param {(content: Object, response: Response) => Object} [successModifier]
 * @param {(error: Error, response: Response) => error} [failureModifier]
 * @returns {Promise<{ response: Response, content: Object }>}
 */

exports.events = events;

function handle(_x) {
  return _handle.apply(this, arguments);
}

function _handle() {
  _handle = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(fetchPromise) {
    var successModifier,
        failureModifier,
        response,
        contentPromise,
        contentType,
        content,
        error,
        _error,
        code,
        message,
        extra,
        _error2,
        _code,
        _message,
        _args = arguments;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            successModifier = _args.length > 1 && _args[1] !== undefined ? _args[1] : function (content) {
              return content;
            };
            failureModifier = _args.length > 2 && _args[2] !== undefined ? _args[2] : function (error) {
              return error;
            };
            _context.prev = 2;
            _context.next = 5;
            return fetchPromise;

          case 5:
            response = _context.sent;
            contentType = response.headers.get('Content-Type') || null;

            if (contentType === null) {
              contentPromise = Promise.resolve(null);
            } else if (contentType.startsWith('application/json')) {
              contentPromise = response.json();
            } else {
              contentPromise = response.text().then(function (text) {
                return {
                  text: text
                };
              });
            }

            _context.next = 10;
            return contentPromise;

          case 10:
            content = _context.sent;
            content = content || {};
            error = null;

            if (content.error && _typeof(content.error) === 'object') {
              error = content.error;
            }

            if (!(error || !response.ok)) {
              _context.next = 21;
              break;
            }

            error = error || content;
            _error = error, code = _error.code, message = _error.message, extra = _objectWithoutProperties(_error, ["code", "message"]);

            if (typeof content.error === 'string') {
              message = message || content.error;
            }

            if (response.status === 400) {
              code = code || 'Invalid';
              message = message || 'Invalid request';
            } else if (response.status === 401) {
              code = code || 'Unauthenticated';
              message = message || 'Unauthenticated';
            } else if (response.status === 403) {
              code = code || 'Unauthorized';
              message = message || 'Unauthorized';
            } else if (response.status === 404) {
              code = code || 'NotFound';
              message = message || 'Not found';
            } else if (response.status >= 500) {
              code = code || 'Server';
              message = message || 'Server error';
            } else {
              code = code || 'Unknown';
              message = message || 'Unknown error';
            }

            error = _error3.FetchError.from(error, _objectSpread({
              code: code,
              message: message
            }, extra));
            throw error;

          case 21:
            content = successModifier(content, response);
            events.emit('success', {
              response: response,
              content: content
            });
            return _context.abrupt("return", {
              response: response,
              content: content
            });

          case 26:
            _context.prev = 26;
            _context.t0 = _context["catch"](2);

            if (_context.t0 instanceof _error3.FetchError) {
              _error2 = _context.t0;
            } else {
              _code = _context.t0.code || 'Unknown';
              _message = _context.t0.message || _context.t0.error || 'Unknown error';
              _error2 = _error3.FetchError.from(_context.t0, {
                code: _code,
                message: _message
              });
            }

            response = response || new Response('http://localhost');
            _error2 = failureModifier(_error2, response);
            events.emit('failure', {
              response: response,
              error: _error2
            });
            throw _error2;

          case 33:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[2, 26]]);
  }));
  return _handle.apply(this, arguments);
}

if (process.env.NODE_ENV === 'development') {
  globalThis.FetchHelper = {
    encode: encode,
    toQueryString: toQueryString,
    toFormData: toFormData,
    Request: Request,
    events: events,
    handle: handle
  };
  events.on('success', function (_ref3) {
    var response = _ref3.response,
        content = _ref3.content;
    Logger.debug('@success', response.url, response.status, content);
  });
  events.on('failure', function (_ref4) {
    var response = _ref4.response,
        error = _ref4.error;
    Logger.debug('@failure', response.url, response.status, error.code, error, error.extra);
  });
}
//# sourceMappingURL=fetch.helper.js.map