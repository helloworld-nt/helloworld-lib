export class BaseError extends Error {
    /**
     *
     * @param {Error & { code: string; extra: Object; }} err
     * @param {Object} [override]
     * @param {string} [override.code]
     * @param {string} [override.message]
     * @param {Object} [override.extra]
     */
    static from(err: Error & {
        code: string;
        extra: any;
    }, { code, message, extra, ...more }?: {
        code: string;
        message: string;
        extra: any;
    }): BaseError;
    constructor(code: any, message?: string, extra?: {});
    code: any;
    extra: {};
}
export class FailureError extends BaseError {
    constructor(...args: any[]);
}
export class FetchError extends FailureError {
    constructor(...args: any[]);
}
export class ValidationError extends FailureError {
    constructor(...args: any[]);
}
