/**
 * Query String and Form Data
 */
/**
 *
 * @param {URLSearchParams|FormData} result
 * @param {string} name
 * @param {*} value
 * @returns {void}
 */
export function encode(result: URLSearchParams | FormData, name: string, value: any): void;
/**
 *
 * @param {Object} data
 * @returns {string}
 */
export function toQueryString(data: any): string;
/**
 *
 * @param {Object} data
 * @returns {FormData}
 */
export function toFormData(data: any): FormData;
/**
 *
 * @param {string} method
 * @param {string} url
 * @param {Object} options
 * @returns {Request}
 */
export function Request(method: string, url: string, options?: any): Request;
/**
 * Handle fetch request, converts JSON to Object and throws error for non-success statuses
 *
 * @param {Promise<Response>} fetchPromise
 * @param {(content: Object, response: Response) => Object} [successModifier]
 * @param {(error: Error, response: Response) => error} [failureModifier]
 * @returns {Promise<{ response: Response, content: Object }>}
 */
export function handle(fetchPromise: Promise<Response>, successModifier?: (content: any, response: Response) => any, failureModifier?: (error: Error, response: Response) => Error): Promise<{
    response: Response;
    content: any;
}>;
/**
 * Response Listeners
 */
export const events: EventEmitter;
import { EventEmitter } from "./events";
