"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.normalize = normalize;
exports.ROLE = exports.LANGUAGE = exports.DURATION_WEEK = exports.DURATION_DAY = exports.DURATION_HOUR = exports.DURATION_MINUTE = exports.DURATION_SECOND = void 0;

function normalize(value, valueMap, defaultValue) {
  var values = Object.values(valueMap);
  return values.includes(String(value).trim()) ? value : defaultValue || values[0];
}
/**
 * Time Constants
 */


var DURATION_SECOND = 1000;
exports.DURATION_SECOND = DURATION_SECOND;
var DURATION_MINUTE = 60 * DURATION_SECOND;
exports.DURATION_MINUTE = DURATION_MINUTE;
var DURATION_HOUR = 60 * DURATION_MINUTE;
exports.DURATION_HOUR = DURATION_HOUR;
var DURATION_DAY = 24 * DURATION_HOUR;
exports.DURATION_DAY = DURATION_DAY;
var DURATION_WEEK = 7 * DURATION_DAY;
/**
 * Shared Constants
 */

exports.DURATION_WEEK = DURATION_WEEK;
var LANGUAGE = Object.freeze({
  ENGLISH: 'en',
  FRENCH: 'fr'
});
exports.LANGUAGE = LANGUAGE;
var ROLE = Object.freeze({
  USER: 'user',
  ADMIN: 'admin'
});
exports.ROLE = ROLE;
//# sourceMappingURL=const.js.map