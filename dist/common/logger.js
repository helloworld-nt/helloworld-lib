"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enableLogger = enableLogger;
exports.disableLogger = disableLogger;
exports.createLogger = createLogger;
exports.setupLogger = setupLogger;
exports.Logger = void 0;

var _debug = _interopRequireDefault(require("debug"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function enableLogger(namespace) {
  _debug.default.enable(namespace);
}

function disableLogger() {
  _debug.default.disable();
}

var PREFIX = 'HelloWorld';

function createLogger() {
  var ns = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var ignorePrefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var namespace = ignorePrefix ? ns : "".concat(PREFIX, ":").concat(ns);
  var logger = (0, _debug.default)("".concat(namespace));
  var debug = logger.extend('DEBUG');
  debug.log = console.debug.bind(console);
  var info = logger.extend('INFO');
  info.log = console.info.bind(console);
  var warn = logger.extend('WARN');
  warn.log = console.warn.bind(console);
  var error = logger.extend('ERROR');
  error.log = console.error.bind(console);
  return {
    namespace: namespace,
    logger: logger,
    debug: debug,
    info: info,
    warn: warn,
    error: error
  };
}

var Logger = createLogger(PREFIX, true);
exports.Logger = Logger;

function setupLogger(namespace) {
  PREFIX = namespace;
  Object.assign(Logger, createLogger(namespace, true));
}
//# sourceMappingURL=logger.js.map