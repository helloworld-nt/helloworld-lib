/**
 * Validation
 */
/**
 *
 * @param {string} value
 * @returns {boolean}
 */
export function email(value: string): boolean;
