export function enableLogger(namespace: any): void;
export function disableLogger(): void;
export function createLogger(ns?: string, ignorePrefix?: boolean): {
    namespace: string;
    logger: createDebug.Debugger;
    debug: createDebug.Debugger;
    info: createDebug.Debugger;
    warn: createDebug.Debugger;
    error: createDebug.Debugger;
};
export function setupLogger(namespace: any): void;
export const Logger: {
    namespace: string;
    logger: createDebug.Debugger;
    debug: createDebug.Debugger;
    info: createDebug.Debugger;
    warn: createDebug.Debugger;
    error: createDebug.Debugger;
};
import createDebug from "debug";
