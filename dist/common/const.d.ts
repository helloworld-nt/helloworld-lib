export function normalize(value: any, valueMap: any, defaultValue: any): any;
/**
 * Time Constants
 */
export const DURATION_SECOND: number;
export const DURATION_MINUTE: number;
export const DURATION_HOUR: number;
export const DURATION_DAY: number;
export const DURATION_WEEK: number;
/**
 * Shared Constants
 */
export const LANGUAGE: Readonly<{
    ENGLISH: string;
    FRENCH: string;
}>;
export const ROLE: Readonly<{
    USER: string;
    ADMIN: string;
}>;
