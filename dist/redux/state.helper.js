"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAction = createAction;
exports.createAsyncAction = createAsyncAction;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @typedef {Object} ActionFunction
 * @prop {string} MODULE
 * @prop {string} NAME
 * @prop {string} ID
 * @prop {string} TYPE
 * @prop {function} action
 */

/**
 * Action creator factory for regular operation
 *
 * @template {Function} T
 * @param {string} module
 * @param {string} name
 * @param {T} action
 * @returns {(T | Function) & ActionFunction} input action augmented by helpers
 */
function createAction(module, name) {
  var action = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var ID = "".concat(module, ".").concat(name);
  var TYPE = "".concat(ID, ".ACTION");
  var config = {
    MODULE: module,
    NAME: name,
    ID: ID,
    TYPE: TYPE,
    action: function action() {
      var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _objectSpread({
        type: TYPE
      }, payload);
    }
  };
  var result = Object.assign(action || function () {}, config);
  return result;
}
/**
 * @typedef {Object} AsyncActionFunction
 * @prop {string} MODULE
 * @prop {string} NAME
 * @prop {string} ID
 * @prop {string} REQUEST
 * @prop {function} request
 * @prop {string} SUCCESS
 * @prop {function} success
 * @prop {string} FAILURE
 * @prop {function} failure
 */

/**
 * Action creators factory for typical async operation
 *
 * @template {Function} T
 * @param {string} module
 * @param {string} name
 * @param {T} action
 * @returns {(T | Function) & AsyncActionFunction} input action augmented by helpers
 */


function createAsyncAction(module, name) {
  var action = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var ID = "".concat(module, ".").concat(name);
  var REQUEST = "".concat(ID, ".REQUEST");
  var SUCCESS = "".concat(ID, ".SUCCESS");
  var FAILURE = "".concat(ID, ".FAILURE");
  var config = {
    MODULE: module,
    NAME: name,
    ID: ID,
    REQUEST: REQUEST,
    request: function request() {
      var input = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return _objectSpread({
        type: REQUEST
      }, input);
    },
    SUCCESS: SUCCESS,
    success: function success() {
      var output = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return function (dispatch) {
        dispatch(_objectSpread({
          type: SUCCESS
        }, output));
        return output;
      };
    },
    FAILURE: FAILURE,
    failure: function failure(error) {
      return function (dispatch) {
        dispatch({
          type: FAILURE
        });
        throw error;
      };
    }
  };
  var result = Object.assign(action || function () {}, config);
  return result;
}
//# sourceMappingURL=state.helper.js.map