/**
 * @typedef {Object} ActionFunction
 * @prop {string} MODULE
 * @prop {string} NAME
 * @prop {string} ID
 * @prop {string} TYPE
 * @prop {function} action
 */
/**
 * Action creator factory for regular operation
 *
 * @template {Function} T
 * @param {string} module
 * @param {string} name
 * @param {T} action
 * @returns {(T | Function) & ActionFunction} input action augmented by helpers
 */
export function createAction<T extends Function>(module: string, name: string, action?: T): (Function & ActionFunction) | (T & ActionFunction);
/**
 * @typedef {Object} AsyncActionFunction
 * @prop {string} MODULE
 * @prop {string} NAME
 * @prop {string} ID
 * @prop {string} REQUEST
 * @prop {function} request
 * @prop {string} SUCCESS
 * @prop {function} success
 * @prop {string} FAILURE
 * @prop {function} failure
 */
/**
 * Action creators factory for typical async operation
 *
 * @template {Function} T
 * @param {string} module
 * @param {string} name
 * @param {T} action
 * @returns {(T | Function) & AsyncActionFunction} input action augmented by helpers
 */
export function createAsyncAction<T extends Function>(module: string, name: string, action?: T): (Function & AsyncActionFunction) | (T & AsyncActionFunction);
export type ActionFunction = {
    MODULE: string;
    NAME: string;
    ID: string;
    TYPE: string;
    action: Function;
};
export type AsyncActionFunction = {
    MODULE: string;
    NAME: string;
    ID: string;
    REQUEST: string;
    request: Function;
    SUCCESS: string;
    success: Function;
    FAILURE: string;
    failure: Function;
};
