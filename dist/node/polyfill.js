"use strict";

// @ts-nocheck

/* eslint-disable global-require, no-undef */
if (!globalThis.FormData) {
  // @ts-ignore
  globalThis.FormData = require('form-data');
}

if (!globalThis.fetch) {
  var _require = require('node-fetch'),
      fetch = _require.default,
      Headers = _require.Headers,
      Request = _require.Request,
      Response = _require.Response; // @ts-ignore


  globalThis.fetch = fetch; // @ts-ignore

  globalThis.Headers = Headers; // @ts-ignore

  globalThis.Request = Request; // @ts-ignore

  globalThis.Response = Response;
}
//# sourceMappingURL=polyfill.js.map