"use strict";

require("intl");

require("intl/locale-data/jsonp/en");

require("intl/locale-data/jsonp/fr");

var _this = void 0;

/**
 * globalThis
 */
if (typeof globalThis === 'undefined') {
  var _globalThis = function () {
    // eslint-disable-next-line no-restricted-globals
    if (typeof self !== 'undefined') return self;
    if (typeof window !== 'undefined') return window;
    if (typeof global !== 'undefined') return global;
    if (typeof _this !== 'undefined') return _this;
    throw new Error('Unable to locate global `this`');
  }(); // @ts-ignore


  _globalThis.globalThis = _globalThis;
}
//# sourceMappingURL=polyfill.js.map