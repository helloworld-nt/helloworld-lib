# Hello World Library

![pipeline status](https://gitlab.com/helloworld-nt/helloworld-lib/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/helloworld-nt/helloworld-lib/badges/master/coverage.svg)
![dependencies](https://img.shields.io/david/naderio/helloworld-lib.svg)

A collection of common utilities.

## Modules

- [`common`](./src/common)

## Usage

```sh
# install dependencies
npm install

# lint code
npm run lint

# format code
npm run format

# run tests
npm run test

# run build
npm run build
```
