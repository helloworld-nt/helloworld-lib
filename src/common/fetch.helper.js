import { EventEmitter } from './events';

import { FetchError } from './error';

import { createLogger } from './logger';

const Logger = createLogger('FetchHelper');

/**
 * Query String and Form Data
 */

/**
 *
 * @param {URLSearchParams|FormData} result
 * @param {string} name
 * @param {*} value
 * @returns {void}
 */
export function encode(result, name, value) {
  if (typeof value === 'object' && value) {
    if (value instanceof File) {
      if (result instanceof FormData) {
        result.set(name, value);
      }
    } else if (value instanceof Date) {
      result.set(name, value.toJSON());
    } else if (typeof value.toJSON === 'function') {
      encode(result, name, JSON.parse(value.toJSON()));
    } else if (Array.isArray(value)) {
      for (let index = 0; index < value.length; index += 1) {
        encode(result, `${name}[${index}]`, value[index]);
      }
    } else {
      for (const [key, val] of Object.entries(value)) {
        encode(result, `${name}[${key}]`, val);
      }
    }
  } else if (typeof value === 'undefined') {
    result.set(name, '');
  } else {
    result.set(name, value);
  }
}

/**
 *
 * @param {Object} data
 * @returns {string}
 */
export function toQueryString(data) {
  const result = new URLSearchParams();

  for (const [name, value] of Object.entries(data)) {
    encode(result, name, value);
  }

  return result.toString();
}

/**
 *
 * @param {Object} data
 * @returns {FormData}
 */
export function toFormData(data) {
  const result = new FormData();

  for (const [name, value] of Object.entries(data)) {
    encode(result, name, value);
  }

  return result;
}

/**
 *
 * @param {string} method
 * @param {string} url
 * @param {Object} options
 * @returns {Request}
 */
export function Request(method, url, options = {}) {
  let { route, query, headers, body, ...more } = options;

  route = route || {};
  query = query || {};
  headers = headers || {};

  Object.entries(route).forEach(([param, value]) => {
    url = url.replace(new RegExp(`:${param}`), encodeURIComponent(value));
  });

  const queryString = toQueryString(query);

  url += queryString ? `?${queryString}` : '';

  headers.Accept = 'application/json';

  if (body && typeof body === 'object') {
    if (body instanceof FormData) {
      headers['Content-Type'] = 'multipart/form-data';
    } else if (body instanceof URLSearchParams) {
      headers['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
    } else if (toString.call(body) === '[object Object]' || toString.call(body) === '[object Array]') {
      headers['Content-Type'] = 'application/json';
      body = JSON.stringify(body);
    }
  }

  const request = new globalThis.Request(url, {
    method,
    headers,
    body,
    ...more,
  });

  return request;
}

/**
 * Response Listeners
 */
export const events = new EventEmitter();

/**
 * Handle fetch request, converts JSON to Object and throws error for non-success statuses
 *
 * @param {Promise<Response>} fetchPromise
 * @param {(content: Object, response: Response) => Object} [successModifier]
 * @param {(error: Error, response: Response) => error} [failureModifier]
 * @returns {Promise<{ response: Response, content: Object }>}
 */
export async function handle(fetchPromise, successModifier = (content) => content, failureModifier = (error) => error) {
  let response;

  try {
    response = await fetchPromise;

    let contentPromise;

    const contentType = response.headers.get('Content-Type') || null;

    if (contentType === null) {
      contentPromise = Promise.resolve(null);
    } else if (contentType.startsWith('application/json')) {
      contentPromise = response.json();
    } else {
      contentPromise = response.text().then((text) => ({ text }));
    }

    let content = await contentPromise;

    content = content || {};

    let error = null;

    if (content.error && typeof content.error === 'object') {
      error = content.error;
    }

    if (error || !response.ok) {
      error = error || content;

      let { code, message, ...extra } = error;

      if (typeof content.error === 'string') {
        message = message || content.error;
      }

      if (response.status === 400) {
        code = code || 'Invalid';
        message = message || 'Invalid request';
      } else if (response.status === 401) {
        code = code || 'Unauthenticated';
        message = message || 'Unauthenticated';
      } else if (response.status === 403) {
        code = code || 'Unauthorized';
        message = message || 'Unauthorized';
      } else if (response.status === 404) {
        code = code || 'NotFound';
        message = message || 'Not found';
      } else if (response.status >= 500) {
        code = code || 'Server';
        message = message || 'Server error';
      } else {
        code = code || 'Unknown';
        message = message || 'Unknown error';
      }

      error = FetchError.from(error, { code, message, ...extra });

      throw error;
    }

    content = successModifier(content, response);

    events.emit('success', { response, content });

    return { response, content };
  } catch (failureError) {
    let error;

    if (failureError instanceof FetchError) {
      error = failureError;
    } else {
      const code = failureError.code || 'Unknown';
      const message = failureError.message || failureError.error || 'Unknown error';

      error = FetchError.from(failureError, { code, message });
    }

    response = response || new Response('http://localhost');

    error = failureModifier(error, response);

    events.emit('failure', { response, error });

    throw error;
  }
}

if (process.env.NODE_ENV === 'development') {
  globalThis.FetchHelper = {
    encode,
    toQueryString,
    toFormData,
    Request,
    events,
    handle,
  };

  events.on('success', ({ response, content }) => {
    Logger.debug('@success', response.url, response.status, content);
  });

  events.on('failure', ({ response, error }) => {
    Logger.debug('@failure', response.url, response.status, error.code, error, error.extra);
  });
}
