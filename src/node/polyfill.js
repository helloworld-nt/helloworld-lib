// @ts-nocheck
/* eslint-disable global-require, no-undef */

if (!globalThis.FormData) {
  // @ts-ignore
  globalThis.FormData = require('form-data');
}

if (!globalThis.fetch) {
  const { default: fetch, Headers, Request, Response } = require('node-fetch');
  // @ts-ignore
  globalThis.fetch = fetch;
  // @ts-ignore
  globalThis.Headers = Headers;
  // @ts-ignore
  globalThis.Request = Request;
  // @ts-ignore
  globalThis.Response = Response;
}
