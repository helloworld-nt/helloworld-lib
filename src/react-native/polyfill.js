// @ts-nocheck
/* eslint-disable global-require, no-undef */

/**
 * Intl
 */

if (!globalThis.Intl) {
  require('intl');
  // @ts-ignore
  require('intl/locale-data/jsonp/en');
  // @ts-ignore
  require('intl/locale-data/jsonp/fr');
}
